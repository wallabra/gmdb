#!/bin/bash

#########################################
#    GMDB Document Printing Utility     #
# ------------------------------------- #
# (c)2019 Gustavo Ramos Rehermann.      #
#                                       #
# This script print the contents of an  #
# existing GMDB document.               #
#########################################
#
# This script, as well as the rest of the
# GMDB codebase, is licensed under the terms
# of the Copyfree Open Innovation License 0.5.
# See LICENSE for more info.
##

DIR="."
DB=""
ID=""

print_help() {
    cat <<EOF
    GMDB Document Printing Utility.
        
Usage:
        
    $0 [-h]
    $0 [-p <path>] -d <database> -i <id>

Arguments:
    -h          Displays this help message.
    database    The name of the database.
    path        The path to the GMDB container.
                Defaults to '.'.
    id          UUID of document to be edited.

----
(c)2019 Gustavo Ramos Rehermann. Also see LICENSE for
source code licensing info.
        
EOF
}

# shellcheck source=.gmdb.common
source "$(dirname "$0")/.gmdb.common"

while getopts 'd:p:e:i:h' option; do
    case "$option" in
        h) print_help; exit 0 ;;
        :) print_help; exit 1 ;;
        p) DIR="$OPTARG" ;;
        d) DB="$OPTARG" ;;
        i) ID="$OPTARG" ;;
        *) print_help; exit 1 ;;
        
    esac
done
shift $((OPTIND - 1))


# Sanity checks.
if [[ -z "$DB" ]]; then
    >&2 cat <<EOF
Please supply a database name via the -d
argument!

Aborting.
EOF
    exit 1
    
fi

if [[ -z "$ID" ]]; then
    >&2 cat <<EOF
Please supply the ID of a document, via
the -i argument!

A list of documents can be retrieved via
the following command:

    $ gmdb.list -p "$DIR" -d "$DATABASE"

Aborting.
EOF

    exit 1

fi

# Mount the gmdb image if not mounted yet.
MOUNTED_NOW=false

if [[ ! -f "$DIR/.gmdb-mounted" ]]; then
    if [[ ! -f "$DIR/.gmdb-img.zip" ]]; then
        >&2 cat <<EOF
There is no GMDB image in this
container! Use gmdb.init to
create one.

Aborting.
EOF
        exit 1
    fi

    if [[ ! -f "$DIR/.gmdb" ]]; then
        mkdir -pv "$DIR/.gmdb"
    fi

    #echo Mounting GMDB...
    gmdb-mount "$DIR/.gmdb-img.zip" "$DIR/.gmdb" >/dev/null 2>&1 || {
        >&2 cat <<EOF
Error mounting the .gmdb-img image to the
.gmdb folder!

Aborting.
EOF
        exit 1
    }

    MOUNTED_NOW=true
fi

# Check whether the database exists.
if [[ ! -f "$DIR/.gmdb/list/$DB" ]]; then
    >&2 cat <<EOF
Database $DB not found!

Aborting.
EOF

    exit 1
    
fi

# Edit (or create) the document.
if [[ ! -f "$DIR/.gmdb/repos/$DB/$ID" ]]; then
    {
        echo 'Can'"'"'t edit a document that does not exist!'
        echo
        echo 'Aborting.'
    } >&2

    exit 1
    
fi

cat "$DIR/.gmdb/repos/$DB/$ID"

# Unmount GMDB.
if [[ "$MOUNTED_NOW" == "true" ]]; then
    gmdb-unmount "$DIR/.gmdb" >/dev/null || {
        {
            # ahh, prettier. I wish I had thought of this from the beginning.
            echo 'Unknown error unmounting the database!'
            echo
            echo Aborting.
    
            exit 1
        } >&2
    }
    
fi


# echo Document edited successfully.
exit 0